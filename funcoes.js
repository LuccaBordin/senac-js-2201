function olaMundoNoLog(nome){
    console.log(`Olá ${nome}!`);
}
olaMundoNoLog('Lucca');

const SOMA = function(a, b){
    return a + b;
}

let resultado = SOMA(2,2);
console.log(resultado);

(function(a, b){
    console.log(a + b);
})(3, 3);


let produto = ['inicio','meio','fim','agora sim é o fim'];

function retornaUltimo(vet){
    vet = produto[produto.length - 1];
    return vet;
};

let novoVet = retornaUltimo(produto);
console.log(novoVet);

let aluno = {
    nome: "Amanda", matricula: 12345, 
    setNome: function(n){
        console.log('Função acessada.');
    }
};
aluno.setNome('Você está acessando uma função.');

let hoje = new Date();
let mes = console.log(hoje.getMonth()+1);

function nomeMesAtual(mes){

    const nomes = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    ];
    let objDate = new Date;
    
    return nomes[objDate.getMonth()];
    }

    console.log(nomeMesAtual());